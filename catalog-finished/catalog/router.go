package catalog

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func NewRouter(repository DefaultRepository) *mux.Router {
	r := mux.NewRouter()
	r.Handle("/products", MakeProductsHandler(repository))
	r.Handle("/products/{id}", MakeGetProductHandler(repository))
	return r
}

func MakeProductsHandler(repository DefaultRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		s, _ := json.Marshal(repository.All())
		w.Header().Set("Content-Type", "application/json")
		w.Write(s)
	}
}

func MakeGetProductHandler(repository DefaultRepository) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := mux.Vars(r)
		id, _ := strconv.Atoi(m["id"])
		p := repository.Get(id)
		b, _ := json.Marshal(p)
		w.Header().Set("Content-Type", "application/json")
		w.Write(b)
	}
}
