package catalog

type Product struct {
	Id    int
	Name  string
	Price float64
}
