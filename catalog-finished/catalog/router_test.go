package catalog

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetProducts(t *testing.T) {
	mock := DefaultRepository{}
	router := NewRouter(mock)

	request, _ := http.NewRequest("GET", "/products", nil)
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, "[{\"Id\":1,\"Name\":\"Jacke\",\"Price\":56},{\"Id\":2,\"Name\":\"Hose\",\"Price\":99}]", response.Body.String())
}
