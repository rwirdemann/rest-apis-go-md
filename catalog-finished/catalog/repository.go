package catalog

type DefaultRepository struct {
}

func (this DefaultRepository) All() []Product {
	p1 := Product{Id: 1, Name: "Jacke", Price: 56.00}
	p2 := Product{Id: 2, Name: "Hose", Price: 99.00}

	return []Product{p1, p2}
}

func (this DefaultRepository) Get(id int) Product {
	return Product{Id: 1, Name: "Jacke", Price: 56.00}
}
