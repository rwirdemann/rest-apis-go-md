package main

import (
	"fmt"
	"net/http"

	"bitbucket.org/rwirdemann/live-demo/catalog"
)

func main() {
	fmt.Printf("Service started on port %d...\n", 8080)
	http.ListenAndServe(":8080", catalog.NewRouter(catalog.DefaultRepository{}))
}
