# Walktrough

## Hello World
main, packages, import
CHECK

## Einfacher Http-Server mit /products Handler
http.HandleFunc
	w.Write([]byte("Jacke, Hose"))

http.ListenAndServer
CHECK

## Domain-Object: Product, Array und Json
new folder catalog
new file product.go
package catalog
type Product struct
p1, p2
c := []catalog.Product{p1, p2}
b, _ := json.Marshal(c)
w.Header().Set("Content-Type", "application/json")
CHECK

## Introduce Repository: OO, Methods, FP
new file repository.go
type DefaultRepository struct
func (r DefaultRepository) All() []Product
global in main: var repository catalog.DefaultRepository
CHECK

Globale Variale ist nicht schön

Closure: Funktion, die Zugriff auf ihren Erstellungskontext hat.
Beim Ausführung kann die Funktion auf diesen Kontext zugreifen.

func MakeProcductsHandler(repository catalog.DefaultRepository) http.HandlerFunc
repository -> local var
CHECK

## GET Product Handler
new file "router.go"
move MakeProcductsHandler
CHECK

mux Router einbauen
func NewRouter(repository catalog.DefaultRepository) *mux.Router
main: NewRouter einbauen
CHECK

neuen Handler GetProduct zufügen
```
return func(writer http.ResponseWriter, request *http.Request) {
    m := mux.Vars(request)
	id, _ := strconv.Atoi(m["id"])
    p := repository.Get(id)
    s, _ := json.Marshal(p)
    writer.Write(s)
}

```
CHECK

## Testing: router_test.go
func TestGetProducts(t *testing.T) {
	mock := DefaultRepository{}
	router := NewRouter(mock)

	request, _ := http.NewRequest("GET", "/products", nil)
	response := httptest.NewRecorder()
	router.ServeHTTP(response, request)

	assert.Equal(t, http.StatusOK, response.Code)
	assert.Equal(t, "[{\"Id\":1,\"Name\":\"Jacke\",\"Price\":56},{\"Id\":2,\"Name\":\"Hose\",\"Price\":99}]", response.Body.String())
}
CHECK

fmain
ff		=> fmt.Printf
meth	=> Methode
tf		=> TestFunctiom