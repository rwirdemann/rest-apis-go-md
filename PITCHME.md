# RESTful APIs in Go

Frankfurter Entwicklertag 2018

Ralf Wirdemann

---

![Logo](assets/api-meta-info.png)

---

## As goes cloud, so goes Go!

![Logo](assets/go-survey-2017.png)

---

# GO 101

---

## Statically typed

```   
var i int

s := "Hallo"      // string
f := 3.142        // float

type struct Product {
    Id   int
    Name string
}

p := Product{1, "Schuhe"}
foo(p)
```
@[1]
@[3-4]
@[6-9]
@[11-12]

---

## Small: Only 25 Keywords

<pre class="stretch"><code class="go">    
  break        default          var         interface       select 
  
  case         defer            go          map             struct 

  chan         else             goto        package         switch 

  const        fallthrough      if          range           type

  continue     for              import      return          func
</code></pre>

Note:
- Java hat 50
- Swift hat 58
- C++
- Spec ist 40 Seiten lang

---

## Look, I'm functional

```
func bar(x int) bool {              
    return x == 42                        
}                                                

func foo(f func (x int) bool) bool {      
    return f(42)                       
}     
                                       
func main() {   
    foo(bar)
}       

```
@[1-3]
@[4-6]
@[9-11]

---

## Well, I'm OO too

```
type Figure interface {
    draw()
} 


type Rectangle struct {
    size   int
    border int
}

func (this Rectangle) draw() {
}

r := Rectangle{}
r.draw()
```
@[1-3]
@[6-9]
@[11-12]
@[14-15]

---

### Cross Platform-Binaries

<img src="assets/one-big-binary.png" alt="Drawing" style="width: 780px;"/>

---

# Standard Library

---

# net/http

Package http provides HTTP client and server implementations.

---

``` 
http.Handle("/foo", fooHandler)

func fooHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hey, %q", html.EscapeString(r.URL.Path))
}

log.Fatal(http.ListenAndServe(":8080", nil))
```
@[1]
@[3-5]
@[7]

---

# encoding/json

Package json implements encoding and decoding of JSON as defined in RFC 4627. 

---

``` 
type Message struct {
    Name string
    Body string
}

m := Message{"Alice", "Hello"}
b, err := json.Marshal(m)
b == []byte(`{"Name":"Alice","Body":"Hello"}`)

var n Message
err := json.Unmarshal(b, &n)
n == Message{Name: "Alice", Body: "Hello"}
```
@[1-4]
@[6-8]
@[10-12]

---

# testing

Package testing provides support for automated testing of Go packages. It is intended to be used in concert with the “go test” command, which automates execution of any function of the form.

---

```
func TestAverage(t *testing.T) {
    v := Average([]float64{1,2})
    if v != 1.5 {
        t.Error("Expected 1.5, got ", v)
    }
}

$ go test
PASS
ok      rest-book/chapter11/math      0.032s
```
@[1-6]
@[8-10]

---

# REST 101

---

![Logo](assets/resources.png)

---

![Logo](assets/domain-design-focus.png)

---

## Go Pair Programming

ralf.wirdemann@kommitment.biz